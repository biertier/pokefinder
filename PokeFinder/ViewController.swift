//
//  ViewController.swift
//  PokeFinder
//
//  Created by Tobias Biermeier on 7/4/17.
//  Copyright © 2017 Tobias Biermeier. All rights reserved.
//

import UIKit
import GeoFire
import Firebase

class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    // location Manager variable initialized
    let locationMannager = CLLocationManager()
    // Bool to determine if map was already centered once
    var mapHasCenteredOnce = false
    // GeoFire object created
    var geoFire: GeoFire!
    // GeoFireReference created
    var geoFireRef = DatabaseReference()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set delegate
        mapView.delegate = self
        
        // Follow user on map
        mapView.userTrackingMode = MKUserTrackingMode.follow
        
        // Set GeoFireRef as a Firebase Database reference
        geoFireRef = Database.database().reference()
        
        // Initialize GeoFire with reference to FireBase Database
        geoFire = GeoFire(firebaseRef: geoFireRef)
        
        
    }

    // call authorization function in view did appear
    override func viewDidAppear(_ animated: Bool) {
        
        locationAuthStatus()
        
    }
    
    // Func to get authorization from user
    func locationAuthStatus() {
        
        // If authorization is "When in use" then show the user on the map, otherwise ask for permission
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            locationMannager.requestWhenInUseAuthorization()
        }
    }
    
    // Func to determine if the user has changed the authorization status
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        // Show user location on map if authorization status is "When in use"
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            mapView.showsUserLocation = true
        }
    }
    
    // Custom Func to center map on user location
    func centerMapOnLocation(location: CLLocation) {
        
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, 2000, 2000)
        
        mapView.setRegion(coordinateRegion, animated: true)
        
    }
    
    // When the user location did update center the map once on the user
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        
        if let loc = userLocation.location {
            if !mapHasCenteredOnce {
                centerMapOnLocation(location: loc)
                mapHasCenteredOnce = true
            }
        }
    }
    
    // Create custom Annotation(image for user position)
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        // Create annotation identifier
        let annoIdentifier = "Pokemon"
        
        // Create annotation View
        var annotationView: MKAnnotationView?
        
        // If annotation is of kind MKUserLocation, create the annotation view and set the image
        if annotation.isKind(of: MKUserLocation.self) {
            
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "User")
            annotationView?.image = UIImage(named: "ash")
            
        // Reuse a reusable Annotation
        } else if let deqAnno = mapView.dequeueReusableAnnotationView(withIdentifier: annoIdentifier) {
            
            annotationView = deqAnno
            annotationView?.annotation = annotation
            
        }  else {
            
            // create a new Annotation View if there is no one to reuse
            let av = MKAnnotationView(annotation: annotation, reuseIdentifier: annoIdentifier)
            av.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            annotationView = av
            
        }

        
        if let annotationView = annotationView, let anno = annotation as? PokeAnnotation {
            
            annotationView.canShowCallout = true
            annotationView.image = UIImage(named: "\(anno.pokemonNumber)")
            let btn = UIButton()
            btn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            btn.setImage(UIImage(named: "map"), for: .normal)
            annotationView.rightCalloutAccessoryView = btn
            
        }
        
        return annotationView
        
    }
    
    // Create a function for pokemon sightings, pass in a location and the ID of the pokemon
    func createSighting(forLocation location: CLLocation, withPokemon pokeId: Int) {
        
        geoFire.setLocation(location, forKey: "\(pokeId)")
        
    }
    
    // Create func to show pokemon on the map
    func showSightingsOnMap(location: CLLocation) {
        
        // Show all results within a 2.5km radius of specified location(self GPS)
        let circleQuery = geoFire.query(at: location, withRadius: 2.5)
        
        _ = circleQuery?.observe(GFEventType.keyEntered, with: { (key, location) in
            
            if let key = key, let location = location {
                let anno = PokeAnnotation(coordinate: location.coordinate, pokemonNumber: Int(key)!)
                self.mapView.addAnnotation(anno)
            }
            
        })
        
    }
    
    // Show pokemon if the user pans or zooms(change of region)
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        
        let location = CLLocation(latitude: mapView.centerCoordinate.latitude, longitude: mapView.centerCoordinate.longitude)
        
        showSightingsOnMap(location: location)
        
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        if let anno = view.annotation as? PokeAnnotation {
            
            let place = MKPlacemark(coordinate: anno.coordinate)
            let destination = MKMapItem(placemark: place)
            destination.name = "Pokemon Sighting"
            let regionDistance: CLLocationDistance = 1000
            let regionSpan = MKCoordinateRegionMakeWithDistance(anno.coordinate, regionDistance, regionDistance)
            
            let options = [MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center), MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span), MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving] as [String : Any]
            
            MKMapItem.openMaps(with: [destination], launchOptions: options)
        }
        
    }
    
    // IBOutlet for button pressed
    @IBAction func spotRandomPokemon(_ sender: UIButton) {
        
        //Select the middle of the map and save lat/lon in a CLLocation
        let loc = CLLocation(latitude: mapView.centerCoordinate.latitude, longitude: mapView.centerCoordinate.longitude)
        
        // Create a random number for the pokeID INT
        let rand = arc4random_uniform(150) + 1
        
        // Call createSighting func with above created variables
        createSighting(forLocation: loc, withPokemon: Int(rand))
        
    }

}











